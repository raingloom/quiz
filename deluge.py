#!/usr/bin/env python
# coding: utf-8

# In[54]:


import requests
import json
from connection import with_cursor
import functools
import base64
import urllib
import time


# In[13]:


def q(s):
    """
    Quote any string for direct inclusion in SQL.

    Currently implemented by base64 encoding and decoding.
    """
    return f"from_base64('{s}')"


# In[15]:


@functools.cache
def otdb_categories():
    with urllib.request.urlopen(
        "https://opentdb.com/api_category.php"
    ) as categories_json:
        return json.load(categories_json)["trivia_categories"]


# In[16]:


@with_cursor
def import_categories(cur):
    for entry in otdb_categories():
        print(entry)
        cur.execute(
            """
        insert into categories (cid, name) values (:id, :name)
        """,
            entry,
        )
    # the explicit commit seems to be necessary
    cur.execute("commit")


# In[56]:


def fetch_from_category(cid, n=5):
    time.sleep(4)
    #cid = cat['id']
    url = f"https://opentdb.com/api.php?amount={str(n)}&category={str(cid)},encode=base64"
    return json.loads(requests.get(url).text)["results"]


# In[22]:


def start_new_quiz(name):
    print(f"qzid := get_or_add_quiz({name})")


# In[29]:


def add_current_quiz_to_category(cid):
    print(f"add_quiz_to_category(qzid,{cid})")


# In[40]:


def add_question(j0):
    print(f"""
    qsid := add_question_with_answers(
    {q(j0["question"])},
    {q(j0["correct_answer"])},
    incorrect_answers_t({','.join(map(q,j0['incorrect_answers']))}));
    insert into quizQuestions (qsid, qzid) values (qsid, qzid);
    """)


# In[48]:


def add_questions(j):
    for j0 in j:
        add_question(j0)


# In[50]:


def prelude():
    print("""
    begin
    qsid questions.qsid%type;
    qzid quizes.qzid%type;
    """)


# In[51]:


def nocturne():
    print("""
    end;
    /""")


# In[58]:

print("-- started import")
prelude()
for cat in otdb_categories():
    print("--", cat)
    start_new_quiz("QUIZ")
    add_questions(fetch_from_category(cat['id']))
nocturne()
