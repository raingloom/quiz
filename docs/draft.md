# Adatbázis alapú rendszerek
* 2022-2023/2
* IB152L-7
* SmartCryptoQuizotron 9000+

## Készítette:
* Gyertyás Marcell Imre
* Hegyi Márton
* Prónai Péter

## Munka felosztása
### Hegyi Márton
Egyedmodell és E-K diagrammok, adatbázisstruktúra vázlat. Néhány nézettábla megvalósítása, triggerek. Táblák leírása

### Prónai Péter
Elején: szervezés, git repo kezelése, adatbázisstruktúra vázlat, dokumentáció összeszerkesztése, adatfolyam diagramok, visszajelzések az egyedmodell és E-K diagrammokra, követelménykatalógus.
Később: adatbázisséma SQL nagy része, adatok nagy része, adat importer, backend architektúra főbb elemei, frontend főbb elemei.

### Gyertyás Marcell
Adatbázisstruktúra vázlat, szerep funkció mátrix, egyed esemény mátrix, visszajelzések az egyedmodell és E-K diagrammokra. Create, read, update, delete műveletek megvalósítása bizonyos táblákon. Bemutatáshoz szükséges kezdeti adatok feltöltése. Alkalmazás tesztelése. Adatbázis táblák tervezése, lekérdezések implementálása.

## Értékelési mód:
Egyéni
<!--
A csoportok az alábbi két értékelési módból választhatnak:
    • A csoport tagjai közösen dolgoznak, minden tag ugyanannyi pontot kap. (Csapat)
    • A csoport tagjai felosztják a munkát, értékelés egyénenként. (Egyéni)
Feladat szöveges leírása
-->

## Leírás
Webes kvízprogramot építünk, ahol felhasználók regisztráció után kérdéssorokat tölthetnek ki, megnézhetik korábbi eredményeiket, adhatnak értékeléseket a kérdésekre, és írhatnak új kérdéssorokat.

## Követelménykatalógus
### Funkcionális:
1. Kérdések hozzáadása
2. Kérdések szerkesztése
3. Válaszok hozzáadása
4. Válaszok szerkesztése
5. Kérdéssorok összerakása
6. Kérdéssorok kategorizációja
7. Regisztráció
9. Bejelentkezés
10. Kérdéssor kitöltése bejelentkezés után, felhasználónként legfeljebb egy aktív kitöltési folyamattal
12. Pontszámok számolása kérdéssoronként és kategóriánként
13. Kérdések utólagos szerkesztésekor a pontszámok újraszámítása
14. Kitöltés közbeni szerkesztés jól legyen kezelve

### Nem funkcionális:
* Mindezt gyorsan csinálja
* Sok párhuzamos kapcsolatot tudjon kezelni.

## Adatfolyam diagram (DFD):
### Logikai
#### 1. szint
![DFD 1. szintje](DFD-Page-1.drawio.png)
#### 2. szint
![DFD 2. szint](DFD-Page-2.drawio.png)

## Egyedmodell:
![egyedmodell diagram](./Egyedmodell.svg)

## EKT-diagram:
![egyedkapcsolat](./ek.svg)

## Relációs adatelemzés
Az eredeti terv már normálformában van.

## Összetett lekérdezések
```sql
create view password_change_count as
select players.username, count(*) as num_of_changes
from password_change, players where password_change.username = players.username group by players.username;
```
Nézettáblát hoz létre ami megmutatja, hogy hányszor változtattot jelszót egy user.

```sql
create view totalCorrectAnswers as select username, sum(correct) as correctAnswers from completions natural join questionAnswerPairs group by username;

create or replace view quizLengths as
select qzid, qname, count(qsid) as length from quizes natural join quizQuestions group by qzid, qname;

create or replace view quizScores as
select qzid, qname, username, count(correct) as correct, length, count(correct)/length as correctRatio
from completions
natural join quizQuestions
natural join questionAnswerPairs
natural join quizLengths
group by qzid, qname, length, username;

create view quizAverages as
select qname, qzid, avg(correctRatio) as averageScore from quizes natural join quizScores group by qzid, qname order by averageScore asc;

create or replace view categoryAverages as
select cid, categories.name as name, avg(averagescore) as averageScore
from quizAverages
natural join quizCategories
natural join categories
group by cid, name
order by averageScore asc;

create or replace view quiz_question_picks as
select questions.qsid, questions.question, count(quizquestions.qsid) as num_of_picked
from quizquestions, questions
where quizquestions.qsid = questions.qsid
group by questions.qsid, questions.question
order by num_of_picked desc;
```

Az összes helye: templates/index.html
## Táblák leírása:
Players: Ebben a táblában tároljuk a felhasználókat.

| **Név** | **Típus** | **Leírás** |
| --- | --- | --- |
| username | varchar2(50) not null primary key | Játékos által megadott név, és egyben a tábla kulcsa |
| password | varchar2(1024) | A játékos által megadott jelszó |
| hash\_algo | varchar2(1024) | A megadott jelszó hash-elése |
| created | date default sysdate | A fiók létrehozásának dátuma |
| questions\_cap | number(1) default 0 |
 |
| answers\_cap | number(1) default 0 |
 |
| players\_cap | number(1) default 0 |
 |

Questions: Ebben a táblában találhatók a kérdések.

| **Név** | **Típus** | **Leírás** |
| --- | --- | --- |
| qsid | integer generated by default on null as identity primary key | A tábla kulcsa |
| question | varchar2(1024) not null | Itt tároljuk a kérdést |

Answers: Ebben a táblában találhatók a válaszok.

| **Név** | **Típus** | **Leírás** |
| --- | --- | --- |
| aid | integer generated by default on null as identity primary key | A tábla kulcsa |
| answer | varchar2(1024) not null unique | Itt tároljuk a kérdésre a választ |

Question-answer pairs: Ebben a táblában tároljuk, hogy egy adott kérdéshez milyen válaszok vannak, és azok közül melyik a helyes.

| **Név** | **Típus** | **Leírás** |
| --- | --- | --- |
| qsid | integer not null | A Questions tábla külsőkulcsa |
| aid | integer not null | Az Answers tábla külsőkulcsa |
| correct | number(1) default 0 | A kérdésre a válasz helyes-e, alapból hamis |

Quizes: Ebben a táblában tároljuk az elérhető kvízeket.

| **Név** | **Típus** | **Leírás** |
| --- | --- | --- |
| qzid | integer generated by default on null as identity primary key | A tábla kulcsa |
| qname | varchar2(512) not null unique | A létrehozott kvíz neve |

Quiz questions: Ebben a táblában tároljuk egy kvízhez tartozó kérdésket.

| **Név** | **Típus** | **Leírás** |
| --- | --- | --- |
| qzid | integer not null | A Quizes tábla kulcsa, külsőkulcs |
| qsid | integer not null | A Questions tábla kulcsa, külsőkulcs |
| pos | integer default 0 | Pozíció a kérdések között |

Categories: Ebben a táblában tároljuk a különböző kvíz kategóroákat.

| **Név** | **Típus** | **Leírás** |
| --- | --- | --- |
| cid | integer generated by default on null as identity primary key | A tábla kulcsa |
| name | varchar(512) not null unique | A kategória neve |
| description | varchar(1024) default '' | A kategória leírása |

Quiz categories: Ebben a táblában tároljuk, hogy egy adott kvíznek milyen a kategóriája

| **Név** | **Típus** | **Leírás** |
| --- | --- | --- |
| cid | integer | A Categories tábla külsőkulcsa |
| qzid | integer | A Quizes tábla külsőkulcsa |

Complaetions: Ebben a táblában tároljuk, hogy egy adott felhasználó egy kvízben melyik kérdésre melyik választ adta.

| **Név** | **Típus** | **Leírás** |
| --- | --- | --- |
| username | varchar2(50) not null | A Players tábla külsőkulcsa |
| qzid | integer not null | A Quizes tábla külsőkulcsa |
| qsid | integer not null | A Questions tábla külsőkulcsa |
| aid | date default sysdate | Az Answers tábla külsőkulcsa |

Password change: Ebben táblában logoljuk, ha egy játékos megváltoztattatta a jelszavát

| **Név** | **Típus** | **Leírás** |
| --- | --- | --- |
| username | varchar2(50) | A Players tábla külsőkulcsa |
| "password" | varchar2(1024) | A játékos által megadott új jelszó |
| hash\_algo | varchar2(1024) | A megadott jelszó hash-elése |
| "date" | date | A jelszócsere dátuma |


## Szerep-funkció mátrix:
![szerep funkció mátrix](./role-function-matrix.png)

## Egyed-esemény mátrix:
![egyed esemeny mátrix](./egyed-esemeny.png)

<!--
## Funkció megadása
* 1.1 kérdések hozzáadás
  * típusa: lekérdezés, online, felhasználói
  * szerepkörök: admin
  * funkcióleírás: új kérdés felvétele az adatbázisba, bemenete a kérdés szövegként, kimenete a kérdéshez rendelt azonosító
  * követelmény: 1
* etc...
-->
## Fordításhoz/futtatáshoz szükséges eszközök

Python3, flask, cx_oracle, pip
