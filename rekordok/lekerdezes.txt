Number of password changes:

select players.username, count(*) as num_of_changes into v_username, v_num_of_changes
from password_change, players where password_change.username = players.username group by players.username;


Order in a view the number of a question picked:

create or replace view quiz_question_picks as
select questions.qsid, questions.question, count(quizquestions.qsid) as num_of_picked
from quizquestions, questions
where quizquestions.qsid = questions.qsid
group by questions.qsid, questions.question
order by num_of_picked desc;
