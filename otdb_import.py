"""Open Trivia Database importer."""
import requests
import json
from connection import with_cursor
import functools


def q(s):
    """
    Quote any string for direct inclusion in SQL.

    Currently implemented by base64 encoding and decoding.
    """
    return f"from_base64('{s}')"


def add_from_json(cur, j0):
    """Add OTDB result from JSON object to database."""
    sql = f"""
    qsid := add_question_with_answers(
    {j0["question"]},
    {j0["correct_answer"]},
    incorrect_answers_t({','.join(map(q,j0['incorrect_answers']))}));
    """
    return sql


@functools.cache
def otdb_categories():
    with urllib.request.urlopen(
        "https://opentdb.com/api_category.php"
    ) as categories_json:
        return json.load(categories_json)["trivia_categories"]


@with_cursor
def import_categories(cur):
    for entry in otdb_categories():
        print(entry)
        cur.execute(
            """
        insert into categories (cid, name) values (:id, :name)
        """,
            entry,
        )
    # the explicit commit seems to be necessary
    cur.execute("commit")


def fetch_from_category(cid):
    #cid = cat['id']
    url = f"https://opentdb.com/api.php?amount=10&category={str(cid)}"
    j=json.loads(requests.get(url).text)
    j0 = j["results"][0]
    

fetch_from_category(9)
