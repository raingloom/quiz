import hashlib
import base64

DEFAULT_ALGO = "sha256"

# TODO salting


def default_hash(password: bytes) -> str:
    """Hashes password with DEFAULT_ALGO."""
    return str(base64.b64encode(hashlib.sha256(password).digest()))
