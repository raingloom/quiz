"""Application entry point."""
from flask import Flask, render_template, request, redirect, session, url_for, flash
import werkzeug
from connection import with_cursor, with_connection
import authentication
from cx_Oracle import Connection, Cursor
from typing import Any, Dict, Callable, Tuple, List, Iterable, Union

app = Flask(__name__)
app.secret_key = "hunter2"

QueryRow = List[Any]
ColumnDescription = List[str]
DictRowfactory = Callable[[Any], Dict[str, Any]]
Response = Union[werkzeug.Response, str]


def description_for(cur: Cursor) -> List[str]:
    """Column names for a cursor, to be used after cur.execute()."""
    # do a copy, Just In Case (TM)
    return [x[0] for x in cur.description]


def dict_rowfactory_for(cur: Cursor) -> DictRowfactory:
    """Create a rowfactory for cur that turns rows into dicts."""

    def factory(*args: Any) -> Dict[str, Any]:
        return dict(zip((d[0] for d in cur.description), args))

    return factory


def table_columns_and_rows(cx: Connection, table) -> Tuple[ColumnDescription, QueryRow]:
    """Column names and rows as dictionaries with column names as keys."""
    cur = cx.cursor()
    cur.execute("select * from " + table)
    cur.rowfactory = dict_rowfactory_for(cur)
    return description_for(cur), cur.fetchall()


def render_tabular(
        template: str,
        title: str,
        keys: ColumnDescription,
        rows: List[QueryRow],
        **extras: Any,
):
    """Render a template, passing it a title and a list of rows."""
    return render_template(
        template, enumerate=enumerate, title=title, keys=keys, rows=rows, **extras
    )


def render_template_with_table(template: str, cx: Connection, table: str):
    """Render "select * from <table>"."""
    keys, rows = table_columns_and_rows(cx, table)
    return render_tabular(template, table, keys, rows)


def tabular(*args, **kwargs):
    """Template for default table views."""
    return render_template_with_table("tabular.html", *args, **kwargs)


@with_cursor
def has_capability(cap: str, cur: Cursor) -> bool:
    """Check if the user is logged in and has a given capability."""
    username = session.get("username")
    if username is None:
        return False
    if not cap.isalpha():
        raise ValueError("possible SQL injection attempt")
    cap_column = cap + "_cap"
    cur.execute(
        f"""
        select {cap_column}
        from players
        where players.username = :username
        """,
        username=username,
    )
    return cur.fetchone()[0] == 1


@app.route("/delete/<name>", methods=["GET", "POST"])
@with_connection
def delete_from(name: str, cx: Connection):
    if session['username'] == 'admin':
        if request.method == "POST":
            cur = cx.cursor()
            rows = table_columns_and_rows(cx, name)[1]
            idname = list(rows[0].keys())[0]

            for rowid in (list(rows[i].values())[0] for i in selected_indexes(name)):
                cur.execute(
                    "delete from {name} where {idname} = '{rowid}'".format(
                        name=name, idname=idname, rowid=rowid
                    )
                )
            cx.commit()
            return redirect(f"/delete/{name}")
        return render_template_with_table("tabular_delete.html", cx, name)
    else:
        return redirect("/")


@app.route("/update/<name>", methods=["GET", "POST"])
@with_connection
def get_info_for_update(name: str, cx: Connection):
    if session['username'] == 'admin':
        if request.method == "POST":
            return render_template(
                "update_record.html",
                keys=request.form.keys(),
                values=list(request.form.values()),
                title=name,
                enumerate=enumerate,
                len=len,
            )
        return render_template_with_table("tabular_update.html", cx, name)
    else:
        return redirect("/")

def update_expressions_tostr(keys: list, values: list) -> str:
    afterset = ""

    for i, key in enumerate(keys):
        if key != "tblname":
            if i != 0:
                if key != "CREATED":
                    if i == (len(keys) - 1):
                        afterset += f"{key} = '{values[i]}'"
                    else:
                        afterset += f"{key} = '{values[i]}', "

    return afterset


@app.route("/update", methods=["POST"])
@with_connection
def update_record(cx: Connection):
    if session['username'] == 'admin':
        if request.method == "POST":
            cur = cx.cursor()

            table = request.form["tblname"]
            keys = list(request.form.keys())
            values = list(request.form.values())

            cur.execute(
                "update "
                + table
                + " set "
                + update_expressions_tostr(keys, values)
                + " where "
                + keys[1]
                + " = "
                + f"'{values[1]}'"
            )
            cx.commit()
            return redirect(f"/update/{table}")
        else:
            return redirect("/")
    else:
        return redirect("/")


@app.route("/view/<name>")
@with_connection
def tabular_page(name: str, cx: Connection):
    """Render default read-only endpoint for tables."""
    return render_template_with_table("tabular.html", cx, name)


@app.route("/view/categories")
@with_connection
def view_categories(cx: Connection):
    """Render categories with links to individual views of them."""
    if session['username'] == 'admin':
        return render_template_with_table("categories.html", cx, "categories")
    else:
        return redirect("/")


@app.route("/view/category/<cid>")
@with_cursor
def view_category(cid: str, cur: Cursor):
    """View the list of quizes in a given category."""
    if session['username'] == 'admin':
        cur.execute(
            """
        select quizes.qname as name
        from categories, quizCategories, quizes
        where categories.cid = :cid and quizCategories.cid = :cid and quizes.qzid = quizCategories.qzid
        """,
            cid=int(cid),
        )
        cur.rowfactory = dict_rowfactory_for(cur)
        return render_tabular(
            "category.html", "Quizes", description_for(cur), cur.fetchall()
        )
    else:
        return redirect("/")


@app.route("/view/quizes")
@with_connection
def view_quizes(cx: Cursor):
    """View a list of all quizes."""
    return render_template_with_table("quizes.html", cx, "quizes")


@app.route("/view/quiz/<int:qzid>")
@with_cursor
def view_quiz(qzid: int, cur: Cursor) -> Response:
    cur.execute(
        """
    select qsid, question
    from quizQuestions natural join questions
    where qzid = :qzid
    """,
        qzid=qzid,
    )
    cur.rowfactory = dict_rowfactory_for(cur)
    return f"""<p><a href="/complete/{qzid}">complete it here</a></p>
    <p><a href="/set_categories/{qzid}">set its categories here</a></p>
    <p><a href="/add_to_quizes">add questions to it and others here</a></p>
    """ + render_tabular(
        "tabular.html", "Quiz", description_for(cur), cur.fetchall()
    )


@app.route("/")
@with_cursor
def index(cur: Cursor):
    """Render the landing page."""

    def table_rows(tbl):
        # TODO sanitize table name?
        cur.execute("select count(*) from " + tbl)
        return cur.fetchone()[0]

    return render_template(
        "index.html",
        questions_count=table_rows("questions"),
        answers_count=table_rows("answers"),
        players_count=table_rows("players"),
        quizes_count=table_rows("quizes"),
        categories_count=table_rows("categories"),
    )


@app.route("/register", methods=["GET", "POST"])
@with_cursor
def register(cur: Cursor):
    """Render the registration form and handles registration."""
    if request.method == "GET":
        return render_template("register.html")
    elif request.method == "POST":
        form = request.form
        name = form["username"]
        password = form["newpass"]
        cur.execute(
            """
        insert into players (username,password,hash_algo)
        values (:username, :password, :hash_algo)
        """,
            [
                name,
                authentication.default_hash(bytes(password, "utf8")),
                authentication.DEFAULT_ALGO,
            ],
        )
        cur.execute("commit")
        return redirect("/")


@app.route("/login", methods=["GET", "POST"])
@with_cursor
def login(cur: Cursor):
    """Render the login form and handles logging in."""
    if request.method == "POST":
        print(session)
        if session.get("username") is not None:
            return redirect(url_for("index"))
        cur.execute(
            """
        select password, hash_algo from players
        where players.username = :username
        """,
            username=request.form["username"],
        )
        pw_and_hash = cur.fetchone()
        if pw_and_hash is not None:
            password_hash, hash_algo = pw_and_hash
            result_hash = authentication.default_hash(
                bytes(request.form["password"], "utf8")
            )
            print(result_hash, password_hash)
            if (
                    hash_algo is not None
                    and hash_algo.lower() != authentication.DEFAULT_ALGO
            ):
                raise Exception("unhandled hash algorithm")
            if password_hash is None or result_hash == password_hash:
                session.clear()
                session["username"] = request.form["username"]
                return redirect(url_for("index"))
        flash("login failed")
    return render_template("login.html")


@app.route("/logout")
def logout():
    """Remove the username from the session if it's there."""
    session.pop("username", None)
    return redirect(url_for("index"))


@app.route("/add_answer", methods=["GET", "POST"])
@with_connection
def add_answer(cx: Connection):
    """Render page where users can add new answers and handle additions."""
    if session['username'] == 'admin':
        cur = cx.cursor()
        if request.method == "POST":
            aid = cur.var(int)
            cur.execute(
                """
            begin
            :aid := get_or_add_answer(:answer);
            end;
            """,
                aid=aid,
                answer=request.form["answer"],
            )
            cx.commit()
            return render_template(
                "inserted.html", added_what="answer", added_id=aid.getvalue()
            )
        return render_template("add_answer.html")
    else:
        return redirect("/")


@app.route("/add_question", methods=["GET", "POST"])
@with_connection
def add_question(cx: Connection):
    """Render page where new questions are added and handle additions."""
    if session['username'] == 'admin':
        cur = cx.cursor()
        if request.method == "POST":
            qsid = cur.var(int)
            cur.execute(
                """
            begin
            :qsid := add_question_with_answers(:question,:canswer,incorrect_answers_t(:answer1,:answer2,:answer3));
            end;
            """,
                qsid=qsid,
                question=request.form["question"],
                canswer=request.form["canswer"],
                answer1=request.form["answer1"],
                answer2=request.form["answer2"],
                answer3=request.form["answer3"],
            )
            cx.commit()
            return render_template(
                "inserted.html", added_what="question", added_id=qsid.getvalue()
            )
        return render_template("add_question.html")
    else:
        return redirect("/")


def selected_indexes(prefix: str) -> Iterable[int]:
    """Get an iterator of selected items matching a given prefix."""
    return iter(
        int(k[len(prefix):]) for k in request.form.keys() if k.startswith(prefix)
    )


@app.route("/create_quiz", methods=["GET", "POST"])
@with_connection
def create_quiz(cx) -> Response:
    """Render page for adding a new quiz and handle its addition."""
    if session['username'] == 'admin':
        if request.method == "POST":
            cx.cursor().execute(
                """
            insert into quizes (qname) values (:qname)
            """,
                qname=request.form["name"],
            )
            cx.commit()
            return redirect("/view/quizes")
        return render_template("create_quiz.html")
    else:
        return redirect("/")


@app.route("/add_to_quizes", methods=["GET", "POST"])
@with_connection
def add_to_quizes(cx: Connection) -> Response:
    """Render page where questions can be added to quizes and handle their addition."""
    if session['username'] == 'admin':
        cur = cx.cursor()
        if request.method == "POST":
            # iterate over questions only once, no need to allocate a list
            questions = table_columns_and_rows(cx, "questions")[1]
            # there are fewer quizes than questions, iterate over this multiple times, cache as list
            quizes = list(table_columns_and_rows(cx, "quizes")[1])
            for question in (questions[i] for i in selected_indexes("questions")):
                for quiz in quizes:
                    # TODO move this into a stored procedure
                    cur.execute(
                        """
                        begin
                            insert into quizQuestions (qzid, qsid) values (:qzid, :qsid);
                        exception
                            when dup_val_on_index then
                                null;
                        end;
                        """,
                        qzid=quiz["QZID"],
                        qsid=question["QSID"],
                    )
            cx.commit()
            return redirect("/view/quizes")
        return (
                "<form method=post>"
                + render_template_with_table("tabular_select.html", cx, "quizes")
                + render_template_with_table("tabular_select.html", cx, "questions")
                + "<input type=submit></form>"
        )
    else:
        return redirect("/")


@app.route("/add_category", methods=["GET", "POST"])
@with_connection
def add_category(cx) -> Response:
    """Render page where categories are added and handle their addition."""
    if session['username'] == 'admin':
        cur = cx.cursor()
        if request.method == "POST":
            cid = cur.var(int)
            cur.execute(
                """
            begin
            :cid := get_or_add_category(:category);
            end;
            """,
                cid=cid,
                category=request.form["category"],
            )
            return render_template(
                "inserted.html", what="category", added_id=cid.getvalue()
            )
        return render_template("add_category.html")
    else:
        return redirect("/")


@app.route("/set_categories/<int:qzid>", methods=["GET", "POST"])
@with_connection
def set_quiz_categories(qzid: int, cx: Connection) -> Response:
    """Render page where you can select which categories a page should be in."""
    if session['username'] == 'admin':
        cur = cx.cursor()
        if request.method == "POST":
            cids = [category["CID"] for category in categories()]
            selected = {cids[i] for i in selected_indexes("set_categories_SELECTED_")}
            unselected = set(cids).difference(selected)
            cur.executemany(
                """
                begin
                    insert into quizCategories (cid, qzid) values (:cid, :qzid);
                exception
                    when dup_val_on_index then
                        null;
                end;
                """,
                [(cid, qzid) for cid in selected],
            )
            cur.executemany(
                """
                delete from quizCategories where cid = :cid and qzid = :qzid
                """,
                [(cid, qzid) for cid in unselected],
            )
            cx.commit()
        cur.execute(
            """
            select (case when (categories.cid in (select cid from quizCategories where quizCategories.qzid = :qzid)) then 1 else 0 end) as selected, categories.* from categories
            """,
            qzid=qzid,
        )
        cur.rowfactory = dict_rowfactory_for(cur)
        cols = description_for(cur)
        rows = cur.fetchall()
        return (
                "<form method=post><input type=submit value=commit>"
                + render_tabular(
            "tabular_select_modify.html",
            "set_categories",
            cols,
            rows,
            selectables={"SELECTED"},
        )
                + "</form>"
        )
    else:
        return redirect("/")


@app.route("/set_answers/<int:qsid>", methods=["GET", "POST"])
@with_connection
def set_question_answers(qsid: int, cx: Connection) -> Response:
    """Render page where you can select which categories a page should be in."""
    if session['username'] == 'admin':
        cur = cx.cursor()
        cur.execute(
            """
            select
            aid,
            (select correct
            from questionAnswerPairs
            where questionAnswerPairs.aid=answers.aid and qsid = :qsid
            ) as correct,
            answer,
            (case
            when (aid,:qsid) in
            (select aid,qsid from questionAnswerPairs)
            then 1
            else 0
            end) as selected
            from answers
            """,
            qsid=qsid,
        )
        cur.rowfactory = dict_rowfactory_for(cur)
        cols = description_for(cur)
        rows = cur.fetchall()
        if request.method == "POST":
            rows = table_columns_and_rows(cx, "answers")[1]
            selected_possible = {
                rows[i]["AID"] for i in selected_indexes("set_answers_SELECTED_")
            }
            selected_correct = {
                rows[i]["AID"] for i in selected_indexes("set_answers_CORRECT_")
            }
            cur.execute("delete from questionAnswerPairs where qsid = :qsid", qsid=qsid)
            cur.executemany(
                "insert into questionAnswerPairs (qsid, aid, correct) values (:qsid, :aid, :correct)",
                [(qsid, aid, aid in selected_correct) for aid in selected_possible],
            )
            cx.commit()
            # kinda ugly hack but it works
            # we do a tail call recursion to re-run the query and render the page
            request.method = "GET"
            return set_question_answers(qsid)
        return (
                "<form method=post><input type=submit value=commit>"
                + render_tabular(
            "tabular_select_modify.html",
            "set_answers",
            cols,
            rows,
            selectables={"CORRECT", "SELECTED"},
        )
                + "</form>"
        )
    else:
        return redirect("/")


@with_cursor
def quiz_name(qzid: int, cur: Cursor) -> str:
    cur.execute("select qname from quizes where qzid = :qzid", qzid=qzid)
    return cur.fetchone()[0]


@app.route("/complete/<int:qzid>", methods=["GET", "POST"])
@with_connection
def complete_quiz(qzid: int, cx: Connection) -> Response:
    """Complete a quiz."""
    if "username" not in session:
        flash("you need to be logged in")
        return redirect(url_for("index"))
    cur = cx.cursor()
    username = session["username"]
    if request.method == "POST":
        cur.execute(
            "delete from completions where username = :username and qzid = :qzid",
            username=username,
            qzid=qzid,
        )
        cur.executemany(
            """
            insert into completions (username, qzid, qsid, aid)
            values (:username, :qzid, :qsid, :aid)
            """,
            [
                (username, qzid, int(qsid), int(aid))
                for qsid, aid in request.form.items()
            ],
        )
        cx.commit()
        return redirect("/view/completions")
    cur.execute(
        """
        select qsid, aid, question, answer
        from questions
        natural join quizQuestions
        natural join questionAnswerPairs
        natural join answers
        where qzid = :qzid
        order by qsid, pos
        """,
        qzid=qzid,
    )
    cur.rowfactory = dict_rowfactory_for(cur)
    cols = description_for(cur)
    rows = cur.fetchall()
    return (
            "<form method=post><input type=submit value=commit>"
            + render_tabular("answer_quiz.html", "fuck", cols, rows)
            + "</form>"
    )


@app.route("/playground")
@with_cursor
def playground(cur):
    """Render an endpoint for one-off tests."""
    return str(categories())


@with_connection
def categories(cx) -> List[Dict[str, Union[int, str]]]:
    """Get list of categories that have been added."""
    return table_columns_and_rows(cx, "categories")[1]


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)
