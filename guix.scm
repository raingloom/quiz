(use-modules
 (gnu packages check)
 (gnu packages python)
 (gnu packages python-check)
 (gnu packages python-web)
 (gnu packages python-xyz)
 (guix build-system python)
 (guix build-system gnu)
 (guix build-system meson)
 (gnu packages compression)
 (gnu packages tls)
 (guix download)
 (guix git-download)
 ((guix licenses) #:prefix license:)
 (guix packages)
 (guix profiles)
 (guix gexp)
 (gnu packages linux)
 (gnu packages pkg-config)
 (gnu packages lua)
 (gnu packages man))

(define-public odpic
  (package
   (name "odpic")
   (version "4.6.0")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/oracle/odpi")
           (commit (string-append "v" version))))
     (sha256
      (base32
       "0dz4aj56zzapsdp0qixqigsqhj8rma0ng1dz48a1m4c99a7vl5w6"))))
   (build-system gnu-build-system)
   (arguments
    (list
     #:tests? #f
     #:make-flags #~(list (string-append "PREFIX=" #$output))
     #:phases '(modify-phases %standard-phases
                              (delete 'configure))))
   (home-page "https://oracle.github.io/odpi/")
   (synopsis "Oracle Database Programming Interface for Drivers and Applications")
   (description
    "Library of C code that simplifies access to Oracle Database for
applications written in C or C++. It is a wrapper over Oracle Call
Interface (OCI) that makes applications and language interfaces easier
to develop.")
   (license #f)))

#;
(define-public oracle-instant-client
  (package
   (name "oracle-instant-client")
   (version "")))

(define-public python-cx-oracle
  (package
   (name "python-cx-oracle")
   (version "8.3.0")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "cx_Oracle" version))
            (sha256
             (base32
              "1n94lkqihdga4ai8xypxi6gp6q3l636bjsd4gv4n6524yid22b9v"))))
   (arguments
    (list
     #:phases #~(modify-phases
                 %standard-phases
                 (add-after
                  'unpack 'set-dirs
                  (lambda _
                    (setenv "ODPIC_INC_DIR" #$(file-append odpic "/include"))
                    (setenv "ODPIC_LIB_DIR" #$(file-append odpic "/lib")))))))
   (build-system python-build-system)
   (propagated-inputs (list odpic))
   (home-page "https://oracle.github.io/python-cx_Oracle")
   (synopsis "Python interface to Oracle")
   (description "Python interface to Oracle")
   (license license:bsd-3)))

#;
(define-public apk-tools
  (package
   (name "apk-tools")
   (version "2.12.11")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://git.alpinelinux.org/apk-tools/")
           (commit (string-append "v" version))))
     (sha256
      (base32
       "02l6msklsnm7dv4nfpx7fbgm6yqv95p2p420rrkcdg4c5g6264dy"))))
   (build-system gnu-build-system)
   (arguments
    (list
     #:make-flags
     #~(list
        "LUA=no" ;; needs zlib binding
        (string-append "DESTDIR=" #$output)
        #;
        (string-append "LUA=" #$(file-append lua "/bin/lua"))
        #;
        (string-append "LUA_PC=lua-" #$(version-major+minor (package-version lua))))
     #:phases
     '(modify-phases %standard-phases
                     (replace 'configure
                              (lambda _
                                (substitute* "Makefile"
                                             (("/usr") "")))))))
   (inputs
    (list lua zlib openssl))
   (native-inputs
    (list pkg-config scdoc))
   (home-page "https://wiki.alpinelinux.org/wiki/Alpine_Package_Keeper")
   (synopsis "Alpine Package Keeper")
   (description
    "The Alpine Linux package manager.")
   (license #f)))

(packages->manifest
 (list
  python
  python-black
  python-cx-oracle
  python-flask
  python-hypothesis
  python-lsp-server
  python-mypy
  python-pytest
  libaio))

