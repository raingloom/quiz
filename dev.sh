#!/bin/sh
set -xe
export FLASK_DEBUG=1
export FLASK_APP=quizotron
export FLASK_ENV=development
while true
do
	python3 hello.py || true
	sleep 1
done
