create or replace function get_answer_id(ans answers.answer%type)
    return answers.aid%type
is
    aid answers.aid%type;
begin
    select aid into aid from answers where answers.answer = ans;
    return aid;
end;

create or replace procedure add_answer(ans answers.answer%type)
is begin
    insert into answers (answer) values (ans);
end;

create or replace function get_or_add_answer(ans answers.answer%type)
    return answers.aid%type
is
    aid answers.aid%type;
begin
    aid := get_answer_id(ans);
    return aid;
exception
    when no_data_found then
        add_answer(ans);
        commit;
        aid := get_answer_id(ans);
        return aid;
end;

set serveroutput on;
declare
    aid answers.aid%type;
begin
    aid := get_or_add_answer('fuckkk');
    dbms_output.put_line('aid: ' || aid);
end;

--select get_or_add_answer('test') from dual;