import cx_Oracle
import os
import functools
from cx_Oracle import Connection, Cursor

db_user = os.environ.get("DBAAS_USER_NAME")
db_password = os.environ.get("DBAAS_USER_PASSWORD")
dsn = cx_Oracle.makedsn("localhost", 1521, sid="orania2")
db_connect: str = os.environ.get("DBAAS_DEFAULT_CONNECT_DESCRIPTOR", dsn)
# service_port = port=os.environ.get('PORT', '8080')


def connect() -> Connection:
    """Return a new database connection."""
    return cx_Oracle.connect(user=db_user, password=db_password, dsn=db_connect)


def with_connection(f):
    """Wrap function so its cx parameter is a new connection."""

    @functools.wraps(f)
    def wrapped(*args, **kwargs):
        with connect() as connection:
            return f(*args, cx=connection, **kwargs)

    return wrapped


def with_cursor(f):
    """Wrap function so its cur parameter is a new cursor."""

    @functools.wraps(f)
    @with_connection
    def wrapped(*args, cx=None, **kwargs):
        return f(*args, cur=cx.cursor(), **kwargs)

    return wrapped
